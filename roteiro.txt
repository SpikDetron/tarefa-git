git help: Mostra vários comandos de forma resumida

git help <comando>: Mostra detalhes de um comando específico

git config --global user.name “Seu Nome”
git config --global user.email “seuemaill@gmail.com”   (Usar o mesmo email do gitlab)
 
Para o repositório atual: --local;
Para o Usuário: --global;
Para o Computador: --system

Opcional:
(primeiro testa code -v)
git config --global core.editor 'code --wait' -> Vscode para editor padrão (precisa do code no PATH)


	Se tudo der certo:
	git config --global -e -> Abre o arquivo de configuração no vscode

-------------------------------------------------------


git init: Faz com que a pasta que o git bash está rodando passe a ser a pasta main.

git status: Verifica se na pasta algum arquivo foi modificado, criado e/ou deletado e, se houver alguma alteração, diz que tal arquivo precisa ser adicionado no repositório para fazer commit, caso contrário só falas que está tudo certo e que todos os commits foram feitos.

git add arquivo/diretório: Adiciona determinado arquivo que esteja pendente ao repositório.
git add --all = git add .: Adiciona todos os arquivos que estão pedentes ao repositório.

git commit -m “Primeiro commit”: Cria um commit.

-------------------------------------------------------

git log: Mostra todos os os commit e quem fez as alterações neles.
git log arquivo: Mostra todos os commits em que o arquivo especificado foi modificado.
git reflog: Mostra todos os commits de forma resumida, mostrando os códigos deles e seus nomes.

-------------------------------------------------------

git show: Mostra o últmo commit e as alterações feitos nele.
git show <commit>: Mostra o commit especificado.

-------------------------------------------------------

git diff: Mostra as alterações feitas em todos os arquivos antes e depois das alterações.
git diff <commit1> <commit2>: Mostra as alterações feitas em commits de escolha.

-------------------------------------------------------

git reset --hard <commit>

-------------------------------------------------------

git branch: Mostra todas as branch do repositório e especifica qual está sendo executada.
git branch -r: Mostra todas as branch remotas e especifica qual está sendo executada.
git branch -a: Mostra ambas as branchs do repositório local e as do remoto.
git branch -d <branch_name>: Deleta a branch especificada. Só funciona se a o git merge tenha sido utilizado antes com a branch de mesmo nome. 
git branch -D <branch_name>: Deleta a branch especificada, independente se foi utilizado o git merge antes com a branch de mesmo nome.
git branch -m <nome_novo>: Renomeia a branch que está sendo executada no momento para o nome especificado.
git branch -m <nome_antigo> <nome_novo>: Renomeia uma branch especificada para um nome novo.


-------------------------------------------------------

git checkout <branch_name>: Muda para a branch especificada. 
git checkout -b <branch_name>: Cria uma nova branch e já muda para ela. 

-------------------------------------------------------

git merge <branch_name>: Mescla a branch especificada com a branch que está sendo executada.

-------------------------------------------------------

git clone: Clona o projeto pelo link do repositório para a pasta que foii executado o git bash.
git pull: Puxa todas as alterações do repositório remoto para o repositório local.
git push: Transfere todas as alterações do repositório local para o repositório remoto.

-------------------------------------------------------

git remote -v
git remote add origin <url>
git remote <url> origin

--------------------------------------------------------

Documentação do git:
https://git-scm.com/doc

Playlist GIT:
https://www.youtube.com/playlist?list=PLucm8g_ezqNq0dOgug6paAkH0AQSJPlIe

Vídeo sobre Git: 
https://www.youtube.com/watch?v=kB5e-gTAl_s
